package service

import (
	"context"
	"fmt"
	"sync"

	us "gitlab.com/EMP/user_service/genproto/user"

	l "gitlab.com/EMP/user_service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *UserService) GoCreateUser(ctx context.Context, req *us.UserReq) (*us.UserResp, error) {
	wg := new(sync.WaitGroup)
	wg.Add(2)
	ch := make(chan us.UserResp)
	c := make(chan []*us.AddressResp)
	result := us.UserResp{}
	
	go s.storage.User().GoCreateUser(ctx, ch, wg, req)
	
	go func(c chan []*us.AddressResp,wg *sync.WaitGroup) {
		defer wg.Done()
		user_add := []*us.AddressResp{}
		for _, address := range req.Addre {
			address.UserId = req.UserId
			result, err := s.storage.User().CreateAdress(ctx, address)
			if err != nil {
				fmt.Println(err)
			}
			user_add = append(user_add, result)
		}
		c <- user_add
	}(c,wg)
	
	result = <-ch
	
	result.Addre = <-c
	
	wg.Wait()
	
	return &result, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *us.UpdateUserReq) (*us.UserResp, error){
	result, err :=s.storage.User().UpdateUser(ctx, req)
	if err != nil {
		s.logger.Error("error while update user", l.Any("Error update user", err))
		return &us.UserResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}

	return result, nil
}

func (s *UserService) CreateAdmin(ctx context.Context, req *us.ID) (*us.Empty, error) {
	result, err :=s.storage.User().CreateAdmin(ctx, req)
	if err != nil {
		s.logger.Error("error while create admin", l.Any("error create admin by id", err))
		return &us.Empty{}, status.Error(codes.Internal, "somthing went wrong, please check id")
	}
	return result, nil
}

func (s *UserService) CreateUser(ctx context.Context, req *us.UserReq) (*us.UserResp, error){
	result, err :=s.storage.User().CreateUser(ctx, req)
	if err != nil {
		s.logger.Error("error while inserting user", l.Any("Error insert user", err))
		return &us.UserResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return result, nil
}

func (s *UserService) GetUserAll(ctx context.Context, req *us.AllUserParamsRequest) (*us.Users, error){
	result, err :=s.storage.User().GetUserAll(ctx, req)

	if err != nil {
		s.logger.Error("error while inserting user", l.Any("Error insert user", err))
		return &us.Users{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return result, nil
}

func (s *UserService) GetUserByEmail(ctx context.Context, req *us.Email) (*us.UserResp, error){
	result, err :=s.storage.User().GetUserByEmail(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.UserResp{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetUserByCountry(ctx context.Context, req *us.AdrSu) (*us.Users, error){
	result, err :=s.storage.User().GetUserByCountry(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.Users{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetUserByCity(ctx context.Context, req *us.AdrSu) (*us.Users, error){
	result, err :=s.storage.User().GetUserByCity(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.Users{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetUserByTown(ctx context.Context, req *us.AdrSu) (*us.Users, error){
	result, err :=s.storage.User().GetUserByTown(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.Users{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetUserByAdress(ctx context.Context, req *us.ID) (*us.UserResp, error){
	result, err :=s.storage.User().GetUserByAdress(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.UserResp{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetUserByID(ctx context.Context, req *us.ID) (*us.UserResp, error){
	result, err :=s.storage.User().GetUserByID(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by id", err))
		return &us.UserResp{}, status.Error(codes.Internal, "somthing went wrong, please check id")
	}
	return result, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *us.ID) (*us.Empty, error){
	result, err :=s.storage.User().DeleteUser(ctx, req)
	if err != nil {
		s.logger.Error("error while delete user", l.Any("error delete user by id", err))
		return &us.Empty{}, status.Error(codes.Internal, "somthing went wrong, please check id")
	}
	return result, nil
}


func (s *UserService) CheckField(ctx context.Context, req *us.CheckFieldReq) (*us.CheckFieldRes, error){
	result, err :=s.storage.User().CheckField(ctx, req)
	if err != nil {
		s.logger.Error("error while delete user", l.Any("error delete user by id", err))
		return &us.CheckFieldRes{}, status.Error(codes.Internal, "somthing went wrong, please check id")
	}

	return result, nil
}