package service

import (
	"github.com/jackc/pgx/v5/pgxpool"
	us "gitlab.com/EMP/user_service/genproto/user"
	l "gitlab.com/EMP/user_service/pkg/logger"
	grpcclient "gitlab.com/EMP/user_service/service/grpc_client"
	"gitlab.com/EMP/user_service/storage"
)

type UserService struct {
	us.UnimplementedUserServiceServer
	storage storage.IStorage
	logger  l.Logger
	Client  grpcclient.GrpcClientI
}

func NewUserService(db *pgxpool.Pool, log l.Logger, client grpcclient.GrpcClientI) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		Client:  client,
	}
}
