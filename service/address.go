package service

import (
	"context"

	us "gitlab.com/EMP/user_service/genproto/user"

	l "gitlab.com/EMP/user_service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *UserService) UpdateAddress(ctx context.Context, req *us.UpdateAdress) (*us.AddressResp, error){
	result, err :=s.storage.User().UpdateAddress(ctx, req)
	if err != nil {
		s.logger.Error("err while update address user", l.Any("err update users address", err))
		return &us.AddressResp{}, status.Error(codes.Internal, "something went wrong, please check user address info")
	}
	
	return result, nil
}

func (s *UserService) CreateAdress(ctx context.Context, req *us.AddressReq) (*us.AddressResp, error){
	result, err :=s.storage.User().CreateAdress(ctx, req)
	if err != nil {
		s.logger.Error("err while create address user", l.Any("err insert users address", err))
		return &us.AddressResp{}, status.Error(codes.Internal, "something went wrong, please check user address info")
	}

	return result, nil
}

func (s *UserService) GetAddressByUserID(ctx context.Context, req *us.ID) (*us.Adress, error){
	result, err :=s.storage.User().GetAddressByUserID(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.Adress{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) GetAddressByID(ctx context.Context, req *us.ID) (*us.AddressResp, error){
	result, err :=s.storage.User().GetAddressByID(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.AddressResp{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) Addadress(ctx context.Context, req *us.AddressReq) (*us.AddressResp, error){
	result, err :=s.storage.User().Addadress(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.AddressResp{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}

func (s *UserService) DeleteAddress(ctx context.Context, req *us.ID) (*us.Empty, error){
	result, err :=s.storage.User().DeleteAddress(ctx, req)
	if err != nil {
		s.logger.Error("error while get user", l.Any("error get user by email", err))
		return &us.Empty{}, status.Error(codes.Internal, "something went wrong, please check email")
	}
	return result, nil
}