package repo

import (
	"context"

	us "gitlab.com/EMP/user_service/genproto/user"
	"sync"
)

type UserStorageI interface {
	//User methods
	CreateUser(context.Context, *us.UserReq) (*us.UserResp, error)
	GetUserByEmail(context.Context, *us.Email) (*us.UserResp, error)
	GetUserByCountry(context.Context, *us.AdrSu) (*us.Users, error)
	GetUserByCity(context.Context, *us.AdrSu) (*us.Users, error)
	GetUserAll(context.Context, *us.AllUserParamsRequest) (*us.Users, error)
	GetUserByTown(context.Context, *us.AdrSu) (*us.Users, error)
	GetUserByAdress(context.Context, *us.ID) (*us.UserResp, error)
	GetUserByID(context.Context, *us.ID) (*us.UserResp, error)
	DeleteUser(context.Context, *us.ID) (*us.Empty, error)	
	UpdateUser(context.Context, *us.UpdateUserReq) (*us.UserResp, error)
    CheckField(context.Context, *us.CheckFieldReq) (*us.CheckFieldRes, error)
	CreateAdmin(context.Context, *us.ID) (*us.Empty, error)
	GoCreateUser(context.Context, chan us.UserResp, *sync.WaitGroup, *us.UserReq) (*us.UserResp, error)
	
	//Address methods
	CreateAdress(context.Context, *us.AddressReq) (*us.AddressResp, error)
	UpdateAddress(context.Context, *us.UpdateAdress) (*us.AddressResp, error)
	GetAddressByUserID(context.Context, *us.ID) (*us.Adress, error)
	GetAddressByID(context.Context, *us.ID) (*us.AddressResp, error)
	Addadress(context.Context, *us.AddressReq) (*us.AddressResp, error)
	DeleteAddress(context.Context, *us.ID) (*us.Empty, error)
}