package storage

import (
	"gitlab.com/EMP/user_service/storage/postgres"
	"gitlab.com/EMP/user_service/storage/repo"

	"github.com/jackc/pgx/v5/pgxpool"
)

type IStorage interface {
	User() repo.UserStorageI
}

type storagePg struct {
	db       *pgxpool.Pool
	userResp repo.UserStorageI
}

func NewStoragePg(db *pgxpool.Pool) *storagePg {
	return &storagePg{
		db:       db,
		userResp: postgres.NewUserRepo(db),
	}
}

func (s storagePg) User() repo.UserStorageI {
	return s.userResp
}
