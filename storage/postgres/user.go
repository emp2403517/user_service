package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"
	"sync"
	"github.com/google/uuid"
	//"github.com/jackc/pgx/v4"
	us "gitlab.com/EMP/user_service/genproto/user"
	"github.com/opentracing/opentracing-go"
)


func (r *userRepo) GoCreateUser(ctx context.Context, ch chan us.UserResp,wg *sync.WaitGroup, req *us.UserReq) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GoCreateUser")
	defer trace.Finish()
	var created_at, updated_at time.Time
	result := us.UserResp{}
	defer wg.Done()
	query := `
	INSERT INTO users(
		user_id, 
		profile_image, 
		first_name, 
		last_name, 
		user_name,
		phone_number, 
		user_type,
		email, 
		password, 
		refresh_token)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) 
	RETURNING 
		user_id, 
		profile_image, 
		first_name, 
		last_name, 
		user_name, 
		phone_number,
		user_type,
		email, 
		password, 
		refresh_token, 
		created_at, updated_at
		`
	err := r.db.QueryRow(context.Background(), query,
		req.UserId,
		req.ProfileImage,
		req.FirstName,
		req.LastName,
		req.UserName,
		req.PhoneNumber,
		req.UserType,
		req.Email,
		req.Password,
		req.RefreshToken).Scan(&result.Id,
		&result.ProfileImage,
		&result.FirstName,
		&result.LastName,
		&result.UserName,
		&result.PhoneNumber,
		&result.UserType,
		&result.Email,
		&result.Password,
		&result.RefreshToken,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.UserResp{}, err
	}
	result.CreatedAt = created_at.Format(time.RFC1123)
	result.UpdatedAt = updated_at.Format(time.RFC1123)
	ch <- result
	return &result, nil
}

func (r *userRepo) CreateUser(ctx context.Context, req *us.UserReq) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateUser")
	defer trace.Finish()
	var created_at, updated_at time.Time
	result := us.UserResp{}

	query := `
	INSERT INTO users(
		user_id, 
		profile_image, 
		first_name, 
		last_name, 
		user_name,
		phone_number, 
		user_type,
		email, 
		password, 
		refresh_token)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) 
	RETURNING 
		user_id, 
		profile_image, 
		first_name, 
		last_name, 
		user_name, 
		phone_number,
		user_type,
		email, 
		password, 
		refresh_token, 
		created_at, updated_at
		`
	err := r.db.QueryRow(context.Background(), query,
		req.UserId,
		req.ProfileImage,
		req.FirstName,
		req.LastName,
		req.UserName,
		req.PhoneNumber,
		req.UserType,
		req.Email,
		req.Password,
		req.RefreshToken).Scan(&result.Id,
		&result.ProfileImage,
		&result.FirstName,
		&result.LastName,
		&result.UserName,
		&result.PhoneNumber,
		&result.UserType,
		&result.Email,
		&result.Password,
		&result.RefreshToken,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.UserResp{}, err
	}

	result.CreatedAt = created_at.Format(time.RFC1123)
	result.UpdatedAt = updated_at.Format(time.RFC1123)

	user_add := []*us.AddressResp{}
	for _, address := range req.Addre {
		address.UserId = result.Id
		result, err := r.CreateAdress(ctx ,address)
		if err != nil {
			return &us.UserResp{}, nil
		}
		user_add = append(user_add, result)
	}
	result.Addre = user_add

	return &result, nil
}

func (r *userRepo) UpdateUser(ctx context.Context, req *us.UpdateUserReq) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdateUser")
	defer trace.Finish()
	var id uuid.UUID
	query := `UPDATE 
	users 
SET 
	updated_at=NOW(), profile_image=$1, 
	first_name=$2, 
	last_name=$3, 
	user_name=$4,
	phone_number=$5, 
	email=$6, 
	password=$7
WHERE 
	user_id=$8 AND deleted_at IS NULL RETURNING user_id`

	err := r.db.QueryRow(context.Background(), query,
		req.ProfileImage,
		req.FirstName,
		req.LastName,
		req.UserName,
		req.PhoneNumber,
		req.Email,
		req.Password,
		req.Id).Scan(&id)
	if err != nil {
		return &us.UserResp{}, err
	}
	req.Id = id.String()
	result, err := r.GetUserByID(ctx ,&us.ID{ID: req.Id})
	if err != nil {
		return &us.UserResp{}, err
	}

	return result, nil
}

func (r *userRepo) GetUserByEmail(ctx context.Context, req *us.Email) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByEmail")
	defer trace.Finish()
	result := us.UserResp{}
	var created_at, updated_at time.Time
	query := `SELECT
		user_id,
		profile_image, 
		first_name, 
		last_name, 
		user_name, 
		phone_number,
		user_type,
		email, 
		password, 
		refresh_token, 
		created_at, updated_at
	FROM users 
	WHERE email=$1 AND deleted_at IS NULL`
	err := r.db.QueryRow(context.Background(), query, req.Email).Scan(
		&result.Id,
		&result.ProfileImage,
		&result.FirstName,
		&result.LastName,
		&result.UserName,
		&result.PhoneNumber,
		&result.UserType,
		&result.Email,
		&result.Password,
		&result.RefreshToken,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.UserResp{}, err
	}

	re, err := r.GetAddressByUserID(ctx ,&us.ID{ID: result.Id})
	if err != nil {
		return &us.UserResp{}, err
	}
	result.Addre = re.UserAdress
	result.CreatedAt = created_at.Format(time.RFC1123)
	result.UpdatedAt = updated_at.Format(time.RFC1123)
	return &result, nil
}

func (r *userRepo) GetUserByCountry(ctx context.Context, req *us.AdrSu) (*us.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByCountry")
	defer trace.Finish()
	result := []*us.UserResp{}
	res := us.Users{}
	query := `SELECT 
	id,
	user_id
	FROM 
		useraddress WHERE country=$1 AND deleted_at IS NULL`

	rows, err := r.db.Query(context.Background(), query, req.Somtinhg)

	if err != nil {
		return &us.Users{}, err
	}

	for rows.Next() {
		res := us.AddressResp{}

		err := rows.Scan(
			&res.Id,
			&res.UserId)
		if err != nil {
			return &us.Users{}, err
		}
		last, err := r.GetUserByID(ctx ,&us.ID{ID: res.UserId})
		if err != nil {
			return &us.Users{}, err
		}

		result = append(result, last)
	}
	res.Usersdata = result
	return &res, nil
}

func (r *userRepo) GetUserByCity(ctx context.Context, req *us.AdrSu) (*us.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByCity")
	defer trace.Finish()
	result := []*us.UserResp{}

	query := `SELECT 
	id, 
	user_id, 
	country, 
	city, 
	town, 
	home_num, 
	created_at, 
	updated_at 
	FROM 
		useraddress WHERE city=$1 AND deleted_at IS NULL`

	rows, err := r.db.Query(context.Background(), query, req.Somtinhg)

	if err != nil {
		return &us.Users{}, err
	}

	for rows.Next() {
		res := us.AddressResp{}

		err := rows.Scan(
			&res.Id)
		if err != nil {
			return &us.Users{}, nil
		}

		last, err := r.GetUserByID(ctx ,&us.ID{ID: res.Id})
		if err != nil {
			return &us.Users{}, nil
		}
		result = append(result, last)
	}

	return &us.Users{Usersdata: result}, nil
}

func (r *userRepo) GetUserByTown(ctx context.Context, req *us.AdrSu) (*us.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByTown")
	defer trace.Finish()
	result := []*us.UserResp{}

	query := `SELECT 
	id, 
	user_id, 
	country, 
	city, 
	town, 
	home_num, 
	created_at, 
	updated_at 
	FROM 
		useraddress WHERE town=$1 AND deleted_at IS NULL`

	rows, err := r.db.Query(context.Background(), query, req.Somtinhg)

	if err != nil {
		return &us.Users{}, err
	}

	for rows.Next() {
		res := us.AddressResp{}

		err := rows.Scan(
			&res.Id)
		if err != nil {
			return &us.Users{}, nil
		}

		last, err := r.GetUserByID(ctx ,&us.ID{ID: res.Id})
		if err != nil {
			return &us.Users{}, nil
		}
		result = append(result, last)
	}

	return &us.Users{Usersdata: result}, nil
}

func (r *userRepo) GetUserByID(ctx context.Context, req *us.ID) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByID")
	defer trace.Finish()
	result := us.UserResp{}
	var created_at, updated_at time.Time
	query := `SELECT
		user_id,
		profile_image, 
		first_name, 
		last_name, 
		user_name, 
		phone_number,
		email, 
		password, 
		refresh_token, 
		created_at, updated_at
	FROM users 
	WHERE user_id=$1 AND deleted_at IS NULL`
	err := r.db.QueryRow(context.Background(), query, req.ID).Scan(
		&result.Id,
		&result.ProfileImage,
		&result.FirstName,
		&result.LastName,
		&result.UserName,
		&result.PhoneNumber,
		&result.Email,
		&result.Password,
		&result.RefreshToken,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.UserResp{}, err
	}

	val, err := r.GetAddressByUserID(ctx ,&us.ID{ID: result.Id})
	if err != nil {

		return &us.UserResp{}, err
	}
	result.Addre = append(result.Addre, val.UserAdress...)
	result.CreatedAt = created_at.String()
	result.UpdatedAt = updated_at.String()
	return &result, nil
}

func (r *userRepo) DeleteUser(ctx context.Context, req *us.ID) (*us.Empty, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "DeleteUser")
	defer trace.Finish()
	_, err := r.db.Exec(context.Background(), `UPDATE users SET deleted_at=NOW() WHERE user_id=$1 AND deleted_at IS NULL`, req.ID)
	if err != nil {
		return &us.Empty{}, err
	}
	return &us.Empty{}, nil
}

func (r *userRepo) GetUserByAdress(ctx context.Context, req *us.ID) (*us.UserResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserByAdress")
	defer trace.Finish()
	res, err := r.GetAddressByID(ctx ,req)
	if err != nil {
		return &us.UserResp{}, err
	}

	result, err := r.GetUserByID(ctx , &us.ID{ID: res.UserId})
	if err != nil {
		return &us.UserResp{}, nil
	}
	return result, nil
}

func (r *userRepo) CheckField(ctx context.Context, user *us.CheckFieldReq) (*us.CheckFieldRes, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CheckField")
	defer trace.Finish()
	response := &us.CheckFieldRes{Exists: false}
	query := fmt.Sprintf("SELECT 1 FROM users WHERE %s=$1", user.Field)
	var temp = 0
	err := r.db.QueryRow(context.Background(), query, user.Value).Scan(&temp)
	if err.Error() != "no rows in result set" {
		fmt.Println(err)
		return &us.CheckFieldRes{}, err
	}

	if temp == 1 {
		response.Exists = true
		return response, nil
	}

	return response, nil
}

func (r *userRepo) GetUserAll(ctx context.Context, user *us.AllUserParamsRequest) (*us.Users, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetUserAll")
	defer trace.Finish()
	var (
		create, update time.Time
		response       = us.Users{}
	)
	query := `
		SELECT
			user_id,
			profile_image, 
			first_name, 
			last_name, 
			user_name, 
			phone_number,
			email, 
			password, 
			refresh_token, 
			created_at, updated_at
		FROM
			users
		WHERE 
			deleted_at IS NULL AND first_name ILIKE $1 OR user_name ILIKE $2 OR last_name ILIKE $3
		LIMIT $4 OFFSET $5
	`
	rows, err := r.db.Query(context.Background(), query,
		"%"+user.Keyboard+"%",
		"%"+user.Keyboard+"%",
		"%"+user.Keyboard+"%",
		user.Limit, (user.Page-1)*user.Limit)

	if err == sql.ErrNoRows {
		return &us.Users{}, nil
	}
	for rows.Next() {
		userRes := us.UserResp{}
		err = rows.Scan(
			&userRes.Id,
			&userRes.ProfileImage,
			&userRes.FirstName,
			&userRes.LastName,
			&userRes.UserName,
			&userRes.PhoneNumber,
			&userRes.Email,
			&userRes.Password,
			&userRes.RefreshToken,
			&create,
			&update,
		)
		userRes.CreatedAt = create.Format(time.RFC1123)
		userRes.UpdatedAt = update.Format(time.RFC1123)
		resp, err :=r.GetAddressByUserID(ctx ,&us.ID{ID: userRes.Id})
		userRes.Addre=resp.UserAdress
		if err != nil {
			fmt.Println("error while scaning users list")
			return &us.Users{}, err
		}
		response.Usersdata = append(response.Usersdata, &userRes)
	}
	return &response, nil
}


func (r *userRepo) CreateAdmin(ctx context.Context, req *us.ID) (*us.Empty, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateAdmin")
	defer trace.Finish()
	_, err := r.db.Exec(context.Background(), `UPDATE users SET user_type='admin' WHERE user_id=$1 AND deleted_at IS NULL`, req.ID)
	if err != nil {
		return &us.Empty{}, err
	}
	return &us.Empty{}, nil
}