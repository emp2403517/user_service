package postgres

import (
	"context"
	"time"

	"github.com/google/uuid"
	us "gitlab.com/EMP/user_service/genproto/user"
	"github.com/opentracing/opentracing-go"
)

func(r *userRepo) UpdateAddress(ctx context.Context, req *us.UpdateAdress) (*us.AddressResp, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdateAddress")
	defer trace.Finish()
	var id uuid.UUID
	query:=`UPDATE useraddress SET updated_at=NOW(), country=$1, city=$2, town=$3, home_num=$4 WHERE id=$5 AND deleted_at IS NULL RETURNING id`
	err :=r.db.QueryRow(context.Background(), query, req.Country, req.City, req.Town, req.HomeNum, req.Id ).Scan(&id)
	if err != nil {
		return &us.AddressResp{}, err
	}
	req.Id=id.String()
	result, err :=r.GetAddressByID(ctx ,&us.ID{ID: req.Id})
	if err != nil {
	 return &us.AddressResp{}, err
	}

	return result, nil
}

func(r *userRepo) GetAddressByID(ctx context.Context, req *us.ID) (*us.AddressResp, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetAddressByID")
	defer trace.Finish()
	var created_at, updated_at time.Time
	result:=us.AddressResp{}
	query:=`SELECT 
	id, 
	user_id, 
	country, 
	city, 
	town, 
	home_num, 
	created_at, 
	updated_at 
		FROM 
			useraddress WHERE id=$1 AND deleted_at IS NULL`
	
	err:=r.db.QueryRow(context.Background(), query, req.ID).Scan(
		&result.Id, 
		&result.UserId,
		&result.Country,
		&result.City,
		&result.Town,
		&result.HomeNum,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.AddressResp{}, err
	}

	result.UpdatedAt=updated_at.Format(time.RFC1123)
	result.CreatedAt=created_at.Format(time.RFC1123)
	return &result, nil
}

func(r *userRepo) DeleteAddress(ctx context.Context, req *us.ID) (*us.Empty, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "DeleteAddress")
	defer trace.Finish()
	_, err := r.db.Exec(context.Background(), `UPDATE useraddress SET deleted_at=NOW() WHERE id=$1 AND deleted_at IS NULL`, req.ID)
	if err != nil {
		return &us.Empty{}, err
	}
	return &us.Empty{}, nil
}

func(r *userRepo) GoCreateAdress(ctx context.Context, req *us.AddressReq) (*us.AddressResp, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GoCreateAdress")
	defer trace.Finish()
	id:=uuid.New().String()
	var created_at, updated_at time.Time
	result:=us.AddressResp{}

	query:=`INSERT INTO useraddress(
		id,
		user_id,
		country,
		city,
		town,
		home_num
		) VALUES ($1, $2, $3, $4, $5, $6) RETURNING 
		id,
		user_id, 
		country, 
		city, 
		town, 
		home_num, 
		created_at, 
		updated_at`

	err:=r.db.QueryRow(context.Background(),  query, id, req.UserId, req.Country, req.City, req.Town, req.HomeNum).Scan(
		&result.Id, 
		&result.UserId, 
		&result.Country,
		&result.City,
		&result.Town,
		&result.HomeNum,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.AddressResp{}, err
	}
	
	result.CreatedAt=created_at.Format(time.RFC1123)
	result.UpdatedAt=updated_at.Format(time.RFC1123)
	return &result, nil		
}

func(r *userRepo) CreateAdress(ctx context.Context, req *us.AddressReq) (*us.AddressResp, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreateAdress")
	defer trace.Finish()
	id:=uuid.New().String()
	var created_at, updated_at time.Time
	result:=us.AddressResp{}

	query:=`INSERT INTO useraddress(
		id,
		user_id,
		country,
		city,
		town,
		home_num
		) VALUES ($1, $2, $3, $4, $5, $6) RETURNING 
		id,
		user_id, 
		country, 
		city, 
		town, 
		home_num, 
		created_at, 
		updated_at`

	err:=r.db.QueryRow(context.Background(),  query, id, req.UserId, req.Country, req.City, req.Town, req.HomeNum).Scan(
		&result.Id, 
		&result.UserId, 
		&result.Country,
		&result.City,
		&result.Town,
		&result.HomeNum,
		&created_at,
		&updated_at)
	if err != nil {
		return &us.AddressResp{}, err
	}
	
	result.CreatedAt=created_at.Format(time.RFC1123)
	result.UpdatedAt=updated_at.Format(time.RFC1123)
	return &result, nil		
}

func(r *userRepo) Addadress(ctx context.Context, req *us.AddressReq) (*us.AddressResp, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "Addadress")
	defer trace.Finish()
	result, err:=r.CreateAdress(ctx, req)
	if err != nil {
		return &us.AddressResp{}, err
	}

	return result, nil
}

func(r *userRepo) GetAddressByUserID(ctx context.Context, req *us.ID) (*us.Adress, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetAddressByUserID")
	defer trace.Finish()
	var created_at, updated_at time.Time
	result:=[]*us.AddressResp{}
	query:=`SELECT 
	id, 
	user_id, 
	country, 
	city, 
	town, 
	home_num, 
	created_at, 
	updated_at 
		FROM 
			useraddress WHERE user_id=$1 AND deleted_at IS NULL`
	
	rows, err :=r.db.Query(context.Background(), query, req.ID)	

	if err != nil {
		return &us.Adress{}, err
	}
		
	for rows.Next() {
		res:=us.AddressResp{}
		
		err :=rows.Scan(
			&res.Id, 
			&res.UserId, 
			&res.Country, 
			&res.City, 
			&res.Town, 
			&res.HomeNum, 
			&created_at, 
			&updated_at)
		if err != nil {
			return &us.Adress{}, nil
		}
		res.UpdatedAt=updated_at.Format(time.RFC1123)
		res.CreatedAt=created_at.Format(time.RFC1123)
		result = append(result, &res)
	}	
	
	
	return &us.Adress{UserAdress: result}, nil
}