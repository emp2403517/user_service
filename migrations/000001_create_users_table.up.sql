CREATE TABLE IF NOT EXISTS users(
    user_id UUID NOT NULL PRIMARY KEY,
    profile_image TEXT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    user_name VARCHAR(50) NOT NULL UNIQUE,
    phone_number VARCHAR(50) NOT NULL,
    user_type   VARCHAR(50) NOT NULL,
    email VARCHAR(100),
    password TEXT NOT NULL,
    refresh_token TEXT,
    created_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP(0) WITH TIME zone NULL

);