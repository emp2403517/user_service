CREATE TABLE IF NOT EXISTS useraddress(
    id uuid NOT NULL PRIMARY KEY,
    user_id uuid NOT NULL,
    country VARCHAR(50),
    city VARCHAR(50),
    town VARCHAR(50),
    home_num VARCHAR(50),
    created_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP(0) WITH TIME zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP(0) WITH TIME zone NULL
) 
